import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { Repository } from 'typeorm';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { BillDetail } from 'src/bill_detail/entities/bill_detail.entity';
import { Material } from 'src/materials/entities/material.entity';

@Injectable()
export class BillService {
  constructor(
    @InjectRepository(Bill)
    private billRepository: Repository<Bill>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(BillDetail)
    private billDetailRepository: Repository<BillDetail>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  async create(createBillDto: CreateBillDto) {
    const employee = await this.employeeRepository.findOneBy({
      id: createBillDto.employeeId,
    });
    const newBill = new Bill();
    newBill.shop_name = createBillDto.shop_name;
    newBill.date = new Date().toLocaleDateString();
    newBill.time = new Date().toLocaleTimeString();
    newBill.buy = createBillDto.buy;
    newBill.employee = employee;
    await this.billRepository.save(newBill);

    for (const item of createBillDto.billDetail) {
      const material = await this.materialsRepository.findOneBy({
        mat_id: item.materialId,
      });
      const billDetail = new BillDetail();
      billDetail.material = material;
      billDetail.name = billDetail.material.mat_name;
      billDetail.amount = item.amount;
      billDetail.price = item.price;
      billDetail.total = item.amount * item.price;
      billDetail.bill = newBill;
      material.mat_quantity =
        Number(billDetail.price) + Number(material.mat_quantity);
      material.mat_price_per_unit = billDetail.price;
      await this.billDetailRepository.save(billDetail);
      await this.materialsRepository.save(material);

      newBill.total = newBill.total + billDetail.total;
      newBill.change = newBill.buy - newBill.total;
    }
    return this.billRepository.save(newBill);
  }

  findAll() {
    return this.billRepository.find({ relations: ['employee'],
    select: { employee: { email: true, name: true , id: true} },
  });
  }

  findOne(id: number) {
    return this.billRepository.findOne({
      where: { id: id },
      relations: ['employee', 'billDetail'],
      select: { employee: { email: true, name: true , id: true} },
    });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    return `This action updates a #${id} bill`;
  }

  async remove(id: number) {
    const bill = await this.billRepository.findOneBy({
      id: id,
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return this.billRepository.remove(bill);
  }
}
