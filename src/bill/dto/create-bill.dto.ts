import { IsNotEmpty, IsDate } from 'class-validator';
import { CreateBillDetailDto } from 'src/bill_detail/dto/create-bill_detail.dto';
export class CreateBillDto {
  
  @IsNotEmpty()
  shop_name: string;

  @IsNotEmpty()
  buy: number;

  @IsNotEmpty()
  employeeId: number;

  @IsNotEmpty()
  billDetail: CreateBillDetailDto[];
}
