import { BillDetail } from 'src/bill_detail/entities/bill_detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn({})
  id: number;

  @Column({ length: '128' })
  shop_name: string;

  @Column()
  date: string;

  @Column()
  time: string;

  @Column({ default: 0, type: 'float' })
  total: number;

  @Column({ type: 'float' })
  buy: number;

  @Column({ default: 0, type: 'float' })
  change: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Employee, (employee) => employee.bill)
  employee: Employee;

  @OneToMany(() => BillDetail, (billDetail) => billDetail.bill)
  bill_detail: BillDetail[];
}
