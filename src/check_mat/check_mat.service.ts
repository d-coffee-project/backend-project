import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckMatDto } from './dto/create-check_mat.dto';
import { UpdateCheckMatDto } from './dto/update-check_mat.dto';
import { CheckMat } from './entities/check_mat.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CheckMatDetail } from './entities/check_mat_detail';
import { Employee } from 'src/employees/entities/employee.entity';
import { Material } from 'src/materials/entities/material.entity';

@Injectable()
export class CheckMatService {
  constructor(
    @InjectRepository(CheckMat)
    private checkMatRepository: Repository<CheckMat>,
    @InjectRepository(CheckMatDetail)
    private checkMaterialDetailsRepository: Repository<CheckMatDetail>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}
  async create(createCheckMatDto: CreateCheckMatDto) {
    const employee = await this.employeesRepository.findOneBy({
      id: createCheckMatDto.empId,
    });
    if (!employee) {
      throw new NotFoundException('Employee not found');
    }
    const checkMaterial = this.checkMatRepository.create({
      employee: employee,
      check_mat_date: new Date().toLocaleDateString(),
      check_mat_time: new Date().toLocaleTimeString(),
    });
    await this.checkMatRepository.save(checkMaterial);

    for (const cmd of createCheckMatDto.check_material_detail) {
      const material = await this.materialsRepository.findOneBy({
        mat_id: cmd.matId,
      });
      if (!material) {
        throw new NotFoundException('Material not found');
      }
      const checkMatDetail = this.checkMaterialDetailsRepository.create({
        cmd_name: material.mat_name,
        cmd_qty_last: material.mat_quantity,
        cmd_qty_remain: cmd.mat_qty_remain,
        cmd_qty_expire: cmd.mat_qty_expire,
        check_material: checkMaterial,
        material: material,
      });
      console.log(Number(cmd.mat_qty_remain));
      material.mat_quantity = Number(cmd.mat_qty_remain);

      await this.materialsRepository.save(material);
      await this.checkMatRepository.save(checkMaterial);

      await this.checkMaterialDetailsRepository.save(checkMatDetail);
    }
    return await this.checkMatRepository.findOne({
      where: { check_mat_id: checkMaterial.check_mat_id },
      relations: ['check_material_detail', 'employee'],
    });
  }

  findAll() {
    return this.checkMatRepository.find({
      relations: ['check_material_detail', 'employee'],
    });
  }

  async findOne(id: number) {
    const checkM = await this.checkMatRepository.findOne({
      where: { check_mat_id: id },
      relations: ['check_material_detail', 'employee'],
    });
    if (!checkM) {
      throw new NotFoundException();
    }
    return checkM;
  }

  update(id: number, updateCheckMatDto: UpdateCheckMatDto) {
    return `This action updates a #${id} checkMaterial`;
  }

  async remove(id: number) {
    const checkM = await this.checkMatRepository.findOneBy({
      check_mat_id: id,
    });
    if (!checkM) {
      throw new NotFoundException();
    }
    return this.checkMatRepository.softRemove(checkM);
  }
}
