import { Test, TestingModule } from '@nestjs/testing';
import { CheckMatService } from './check_mat.service';

describe('CheckMatService', () => {
  let service: CheckMatService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckMatService],
    }).compile();

    service = module.get<CheckMatService>(CheckMatService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
