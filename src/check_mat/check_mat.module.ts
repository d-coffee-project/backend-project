import { Module } from '@nestjs/common';
import { CheckMatService } from './check_mat.service';
import { CheckMatController } from './check_mat.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { CheckMat } from './entities/check_mat.entity';
import { CheckMatDetail } from './entities/check_mat_detail';
import { Material } from 'src/materials/entities/material.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Employee, CheckMat, CheckMatDetail, Material]),
  ],
  controllers: [CheckMatController],
  providers: [CheckMatService],
})
export class CheckMatModule {}
