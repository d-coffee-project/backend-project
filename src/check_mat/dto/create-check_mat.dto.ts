import { IsNotEmpty } from 'class-validator';

class CreateCheckMaterialDetailDto {
  @IsNotEmpty()
  matId: number;

  @IsNotEmpty()
  mat_qty_remain: number;

  @IsNotEmpty()
  mat_qty_expire: number;
}
export class CreateCheckMatDto {
  empId: number;

  @IsNotEmpty()
  check_material_detail: CreateCheckMaterialDetailDto[];
}
