import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckMatDto } from './create-check_mat.dto';

export class UpdateCheckMatDto extends PartialType(CreateCheckMatDto) {}
