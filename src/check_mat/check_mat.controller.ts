import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CheckMatService } from './check_mat.service';
import { CreateCheckMatDto } from './dto/create-check_mat.dto';
import { UpdateCheckMatDto } from './dto/update-check_mat.dto';
import { JwtService } from '@nestjs/jwt';

@UseGuards(JwtService)
@Controller('check-mat')
export class CheckMatController {
  constructor(private readonly checkMatService: CheckMatService) {}

  @Post()
  create(@Body() createCheckMatDto: CreateCheckMatDto) {
    return this.checkMatService.create(createCheckMatDto);
  }

  @Get()
  findAll() {
    return this.checkMatService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkMatService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckMatDto: UpdateCheckMatDto,
  ) {
    return this.checkMatService.update(+id, updateCheckMatDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkMatService.remove(+id);
  }
}
