import { Test, TestingModule } from '@nestjs/testing';
import { CheckMatController } from './check_mat.controller';
import { CheckMatService } from './check_mat.service';

describe('CheckMatController', () => {
  let controller: CheckMatController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckMatController],
      providers: [CheckMatService],
    }).compile();

    controller = module.get<CheckMatController>(CheckMatController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
