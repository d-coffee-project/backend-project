import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CheckMat } from './check_mat.entity';
import { Material } from 'src/materials/entities/material.entity';

@Entity()
export class CheckMatDetail {
  @PrimaryGeneratedColumn()
  cmd_id: number;

  @Column()
  cmd_name: string;

  @Column({ default: 0 })
  cmd_qty_last: number;

  @Column({ default: 0 })
  cmd_qty_remain: number;

  @Column({ default: 0 })
  cmd_qty_expire: number;

  @ManyToOne(() => Material, (material) => material.check_material_detail)
  material: Material;

  @ManyToOne(
    () => CheckMat,
    (check_material) => check_material.check_material_detail,
  )
  check_material: CheckMat;
}
