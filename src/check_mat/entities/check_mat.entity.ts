import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  DeleteDateColumn,
  // Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CheckMatDetail } from './check_mat_detail';

@Entity()
export class CheckMat {
  @PrimaryGeneratedColumn()
  check_mat_id: number;

  @Column()
  check_mat_date: string;

  @Column()
  check_mat_time: string;

  @ManyToOne(() => Employee, (employee) => employee.checkMats)
  employee: Employee;

  @DeleteDateColumn()
  deleteAt: Date;

  @OneToMany(
    () => CheckMatDetail,
    (check_material_detail) => check_material_detail.check_material,
  )
  check_material_detail: CheckMatDetail[];
  checkMat: any;
}
