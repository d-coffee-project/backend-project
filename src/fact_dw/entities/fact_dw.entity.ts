import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';


@Entity()
export class FactDw {
  @PrimaryGeneratedColumn()
  Fact_id: number;

  @Column({ type: 'float' })
  amount_fact: number;

  @Column({ type: 'float' })
  total_fact: number;

  @Column({ length: 64 })
  customer_Id: number;

  @Column({ length: 255 })
  employee_Id: number;

  @Column({ length: 255 })
  product_Id: number;

  @Column({ length: 255 })
  store_Id: number;

  @Column({ length: 64 })
  time_Id: Date;

}
