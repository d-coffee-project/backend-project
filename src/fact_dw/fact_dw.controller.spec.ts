import { Test, TestingModule } from '@nestjs/testing';
import { FactDwController } from './fact_dw.controller';
import { FactDwService } from './fact_dw.service';

describe('FactDwController', () => {
  let controller: FactDwController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FactDwController],
      providers: [FactDwService],
    }).compile();

    controller = module.get<FactDwController>(FactDwController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
