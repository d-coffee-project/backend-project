import { Injectable } from '@nestjs/common';
import { CreateFactDwDto } from './dto/create-fact_dw.dto';
import { UpdateFactDwDto } from './dto/update-fact_dw.dto';

@Injectable()
export class FactDwService {
  create(createFactDwDto: CreateFactDwDto) {
    return 'This action adds a new factDw';
  }

  findAll() {
    return `This action returns all factDw`;
  }

  findOne(id: number) {
    return `This action returns a #${id} factDw`;
  }

  update(id: number, updateFactDwDto: UpdateFactDwDto) {
    return `This action updates a #${id} factDw`;
  }

  remove(id: number) {
    return `This action removes a #${id} factDw`;
  }
}
