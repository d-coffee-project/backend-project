import { Test, TestingModule } from '@nestjs/testing';
import { FactDwService } from './fact_dw.service';

describe('FactDwService', () => {
  let service: FactDwService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FactDwService],
    }).compile();

    service = module.get<FactDwService>(FactDwService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
