import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { FactDwService } from './fact_dw.service';
import { CreateFactDwDto } from './dto/create-fact_dw.dto';
import { UpdateFactDwDto } from './dto/update-fact_dw.dto';

@Controller('fact-dw')
export class FactDwController {
  constructor(private readonly factDwService: FactDwService) {}

  @Post()
  create(@Body() createFactDwDto: CreateFactDwDto) {
    return this.factDwService.create(createFactDwDto);
  }

  @Get()
  findAll() {
    return this.factDwService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.factDwService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFactDwDto: UpdateFactDwDto) {
    return this.factDwService.update(+id, updateFactDwDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.factDwService.remove(+id);
  }
}
