import { Module } from '@nestjs/common';
import { FactDwService } from './fact_dw.service';
import { FactDwController } from './fact_dw.controller';
import { FactDw } from './entities/fact_dw.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([FactDw])],
  controllers: [FactDwController],
  providers: [FactDwService]
})
export class FactDwModule {}
