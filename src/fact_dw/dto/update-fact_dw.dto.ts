import { PartialType } from '@nestjs/mapped-types';
import { CreateFactDwDto } from './create-fact_dw.dto';

export class UpdateFactDwDto extends PartialType(CreateFactDwDto) {}
