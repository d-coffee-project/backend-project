import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateStockDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNumber()
  @Min(0)
  stock: number;
}
