import { Employee } from 'src/employees/entities/employee.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: '32' })
  role: string;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    unique: true,
    length: '64',
  })
  email: string;

  @Column({
    length: '128',
  })
  password: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  // @OneToMany(() => Order, (order) => order.user)
  // orders: Order[];

  @OneToOne(() => Employee, (employee) => employee.user)
  employee: Employee;
}
