import { IsNotEmpty } from "class-validator";

export class CreateBillDetailDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  amount: number;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  materialId: number;
}
