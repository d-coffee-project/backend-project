import { Bill } from 'src/bill/entities/bill.entity';
import { Material } from 'src/materials/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class BillDetail {
  @PrimaryGeneratedColumn({})
  id: number;

  @Column({ length: '128' })
  name: string;

  @Column({ type: 'float' })
  amount: number;

  @Column({ type: 'float' })
  price: number;

  @Column({ type: 'float', default: 0 })
  total: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Bill, (bill) => bill.bill_detail,{ onDelete: 'CASCADE' })
  bill: Bill;

  @ManyToOne(() => Material, (material) => material.bill_detail)
  material: Material;
}
