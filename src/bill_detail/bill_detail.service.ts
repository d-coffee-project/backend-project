import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDetailDto } from './dto/create-bill_detail.dto';
import { UpdateBillDetailDto } from './dto/update-bill_detail.dto';
import { BillDetail } from './entities/bill_detail.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Bill } from 'src/bill/entities/bill.entity';
import { Material } from 'src/materials/entities/material.entity';

@Injectable()
export class BillDetailService {
  constructor(
    @InjectRepository(BillDetail)
    private billDetailRepository: Repository<BillDetail>,
    @InjectRepository(Bill)
    private billRepository: Repository<Bill>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  create(createBillDetailDto: CreateBillDetailDto) {
    return 'This action adds a new billDetail';
  }

  findAll() {
    return this.billDetailRepository.find();
  }

  // findOne(id: number) {
  //   return `This action returns a #${id} billDetail`;
  // }

  async findOne(id: number) {
    const billD = await this.billDetailRepository.findOne({
      where: { id:id },
      relations: ['matetrials'],
    });
    if (!billD) {
      throw new NotFoundException();
    }
    return billD;
  }

  update(id: number, updateBillDetailDto: UpdateBillDetailDto) {
    return `This action updates a #${id} billDetail`;
  }

  async remove(id: number) {
    const billDetail = await this.billDetailRepository.findOneBy({
      id: id,
    });
    if (!billDetail) {
      throw new NotFoundException();
    }
    return this.billDetailRepository.remove(billDetail);
  }
}
