import { IsNotEmpty } from 'class-validator';

class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsNotEmpty()
  amount: number;
  @IsNotEmpty()
  price: number;
}
export class CreateOrderDto {
  customerId: number;

  employeeId: number;

  storeId: number;

  recevied: number;

  change: number;

  payment: string;

  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
