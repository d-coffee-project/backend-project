import { Customer } from 'src/customers/entities/customer.entity';
import { Store } from 'src/stores/entities/store.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { OrderItem } from './order-item';
import { Employee } from 'src/employees/entities/employee.entity';
@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amount: number;

  @Column({ type: 'float' })
  total: number;

  @Column({ type: 'float' })
  recevied: number;

  @Column({ type: 'float', default: 0 })
  change: number;

  // @Column({ default: 0 })
  // discount: number;

  @Column()
  payment: string;

  @ManyToOne(() => Customer, (customer) => customer.orders)
  customer: Customer;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
  orders: any;

  // @ManyToOne(() => User, (user) => user.orders)
  // user: User;

  @ManyToOne(() => Store, (store) => store.orders)
  store: Store;

  @ManyToOne(() => Employee, (employee) => employee.orders)
  employee: Employee;
}
