import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class EmployeeDw {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;
}
