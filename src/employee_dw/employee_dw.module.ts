import { Module } from '@nestjs/common';
import { EmployeeDwService } from './employee_dw.service';
import { EmployeeDwController } from './employee_dw.controller';
import { EmployeeDw } from './entities/employee_dw.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([EmployeeDw])],
  controllers: [EmployeeDwController],
  providers: [EmployeeDwService],
})
export class EmployeeDwModule {}
