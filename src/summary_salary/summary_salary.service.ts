import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { CreateSummarySalaryDto } from './dto/create-summary_salary.dto';
import { UpdateSummarySalaryDto } from './dto/update-summary_salary.dto';
import { SummarySalary } from './entities/summary_salary.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class SummarySalaryService {
  constructor(
    @InjectRepository(SummarySalary)
    private summarysalaryRepository: Repository<SummarySalary>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) { }

  create(createSummarySalaryDto: CreateSummarySalaryDto) {
    return this.summarysalaryRepository.save(createSummarySalaryDto);
  }

  findAll() {
    return this.summarysalaryRepository.find({ relations: ['checkinout', 'checkinout.employee'] });
  }

  findUnPaid(st: string) {
    return this.summarysalaryRepository.find({ relations: ['checkinout', 'checkinout.employee'], 
    where: {status: Not(st)} });

    // return this.summarysalaryRepository.find({
    //   relations: ['checkinout', 'checkinout.employee'],
    //   where: {}

    // });
  }

  findOne(id: number) {
    return this.summarysalaryRepository.findOneBy({ id: id });
  }

  async update(id: number, updateSummarySalaryDto: UpdateSummarySalaryDto) {
    const summarysalary = await this.summarysalaryRepository.findOne({
      where: { id: id },
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    summarysalary.date = new Date().toLocaleDateString();
    summarysalary.status = "Paid"
    return this.summarysalaryRepository.save(summarysalary);
  }

  async remove(id: number) {
    const summarysalary = await this.summarysalaryRepository.findOne({
      where: { id: id },
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    return this.summarysalaryRepository.softRemove(summarysalary);
  }
}
