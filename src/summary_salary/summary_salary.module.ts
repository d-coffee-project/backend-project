import { Module } from '@nestjs/common';
import { SummarySalaryService } from './summary_salary.service';
import { SummarySalaryController } from './summary_salary.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SummarySalary } from './entities/summary_salary.entity';
import { CheckInOut } from 'src/check_in_out/entities/check_in_out.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SummarySalary,CheckInOut,Employee])],
  controllers: [SummarySalaryController],
  providers: [SummarySalaryService],
})
export class SummarySalaryModule {}
