import { IsNotEmpty, Min } from 'class-validator';
import { CheckInOut } from 'src/check_in_out/entities/check_in_out.entity';

export class CreateSummarySalaryDto {
  @IsNotEmpty()
  workhour: number;

  @IsNotEmpty()
  salary: number;

  checkinoutId: CheckInOut;
}
