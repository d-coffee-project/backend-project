import { CheckInOut } from 'src/check_in_out/entities/check_in_out.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class SummarySalary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float' })
  workhour: number;

  @Column({ type: 'float', default: 0 })
  salary: number;

  @Column({nullable: true})
  date: String;

  @Column({nullable: true})
  status: String;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  // @OneToMany(() => CheckInOut, (checkinout) => checkinout.summary_salary)
  // checkinout: CheckInOut[];

  @OneToOne(() => CheckInOut, (checkinout) => checkinout.summary_salary)
  @JoinColumn()
  checkinout: CheckInOut;
}
