import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { JwtService } from '@nestjs/jwt';

@UseGuards(JwtService)
@Controller('customers')
export class CustomersController {
  constructor(private readonly customersService: CustomersService) {}

  @Post()
  create(@Body() createCustomerDto: CreateCustomerDto) {
    return this.customersService.create(createCustomerDto);
  }

  @Get()
  findAll() {
    return this.customersService.findAll();
  }

  // @Get()
  // findAll(@Query() query: { cat?: string; order?: string; orderBy?: string }) {
  //   return this.productsService.findAll({
  //     relations: ['category'],
  //     order: query.orderBy
  //       ? { [query.orderBy]: query.order }
  //       : { createdDate: 'ASC' },
  //     where: query.cat ? { categoryId: parseInt(query.cat) } : {},
  //   });
  // }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.customersService.findOne(+id);
  }
  //หาเบอร์เมมเบ้อ
  @Get('cus/:tel')
  findCusByTel(@Param('tel') tel: string) {
    return this.customersService.findCusByTel(tel);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCustomerDto: UpdateCustomerDto,
  ) {
    return this.customersService.update(+id, updateCustomerDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.customersService.remove(+id);
  }

  @Get('customer/:name')
  findByName(@Param('name') name: string) {
    return this.customersService.findByName(name);
  }
}
