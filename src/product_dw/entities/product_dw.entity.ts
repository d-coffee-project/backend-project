import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ProductDw {
  @PrimaryGeneratedColumn()
  product_id: number;

  @Column({ length: 64 })
  category_id: string;

  @Column({ length: 255 })
  product_name: string;

  @Column({ type: 'float' })
  product_price: number;
}
