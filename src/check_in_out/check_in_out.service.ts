import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { User } from 'src/users/entities/user.entity';
import { IsNull, Repository } from 'typeorm';
import { CreateCheckInOutDto } from './dto/create-check_in_out.dto';
import { UpdateCheckInOutDto } from './dto/update-check_in_out.dto';
import { CheckInOut } from './entities/check_in_out.entity';
import { SummarySalary } from 'src/summary_salary/entities/summary_salary.entity';

@Injectable()
export class CheckInOutService {
  constructor(
    @InjectRepository(CheckInOut)
    private checkinoutsRepository: Repository<CheckInOut>,
    @InjectRepository(Employee)
    private EmployeesRepository: Repository<Employee>,
    @InjectRepository(User)
    private UsersRepository: Repository<User>,
    @InjectRepository(SummarySalary)
    private SummarySalaryRepository: Repository<SummarySalary>,
  ) { }

  async create(createCheckInOutDto: CreateCheckInOutDto) {
    const cio = new CheckInOut();
    const employee = await this.EmployeesRepository.findOne({
      where: { email: createCheckInOutDto.email },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    console.log(employee);
    console.log(cio);
    cio.employee = employee;
    return this.checkinoutsRepository.save(cio);
  }

  findAll() {
    return this.checkinoutsRepository.find({
      relations: ['employee'],
      select: { employee: { email: true, name: true , id: true} },
    });
  }

  async findByEmail(email: string) {
    return this.checkinoutsRepository.find({
      relations: ['employee'],
      select: { employee: { email: true , name: true,  id: true} },
      where:{employee: { email: email }}
    });
  }

  async findCheck(email: string) {
    const checkinout = await this.checkinoutsRepository.findOne({
      relations: ['employee'],
      where: { employee: { email: email }, time_out: IsNull() },
    });
    if (!checkinout) {
      return false;
    }
    return true;
  }

  async update(updateCheckInOutDto: UpdateCheckInOutDto) {
    console.log(updateCheckInOutDto.email);
    const check = await this.checkinoutsRepository.findOne({
      where: {
        employee: { email: updateCheckInOutDto.email },
        time_out: IsNull(),
      },
      relations: ['employee'],
    });
    if (!check) {
      throw new NotFoundException();
    }
    const time = new Date();
    check.time_out = time.toLocaleTimeString();
    const timediff = Math.abs(time.getTime() - check.createdDate.getTime());
    const h = Math.floor(timediff / (1000 * 60 * 60));
    const m = Math.floor((timediff / (1000 * 60)) % 60);
    const resultTime = `${h}.${m.toString().padStart(2, '0')}`;
    check.total_hour = parseFloat(resultTime);

    const employee = await this.EmployeesRepository.findOne({
      where: { email: updateCheckInOutDto.email },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    const ss = new SummarySalary();
    ss.workhour = check.total_hour;
    ss.checkinout = check;
    ss.salary = ss.workhour * employee.wage;
    ss.status = "UnPaid"

    const s = await this.SummarySalaryRepository.save(ss);
    check.summary_salary = s;

    return this.checkinoutsRepository.save(check);
  }

  async remove(id: number) {
    const cio = await this.checkinoutsRepository.findOneBy({ id: id });
    if (!cio) {
      throw new NotFoundException();
    }
    return this.checkinoutsRepository.remove(cio);
  }
}
