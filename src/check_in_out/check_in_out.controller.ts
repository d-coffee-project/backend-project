import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckInOutService } from './check_in_out.service';
import { CreateCheckInOutDto } from './dto/create-check_in_out.dto';
import { UpdateCheckInOutDto } from './dto/update-check_in_out.dto';

@Controller('check-in-out')
export class CheckInOutController {
  constructor(private readonly checkInOutService: CheckInOutService) {}

  @Post()
  create(@Body() createCheckInOutDto: CreateCheckInOutDto) {
    return this.checkInOutService.create(createCheckInOutDto);
  }

  @Patch()
  checkout(@Body() updateCheckInOutDto: UpdateCheckInOutDto) {
    return this.checkInOutService.update(updateCheckInOutDto);
  }

  @Get()
  findAll() {
    return this.checkInOutService.findAll();
  }

  @Get(':email')
  findMail(@Param('email') email: string) {
    return this.checkInOutService.findByEmail(email);
  }

  @Get('check/:email')
  findBymail(@Param('email') email: string) {
    return this.checkInOutService.findCheck(email);
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.checkInOutService.findOne(+id);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateCheckInOutDto: UpdateCheckInOutDto,
  // ) {
  //   return this.checkInOutService.update(updateCheckInOutDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkInOutService.remove(+id);
  }
}
