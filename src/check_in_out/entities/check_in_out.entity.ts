import { Employee } from 'src/employees/entities/employee.entity';
import { SummarySalary } from 'src/summary_salary/entities/summary_salary.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckInOut {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: new Date().toLocaleDateString() })
  date: string;

  @Column({ default: new Date().toLocaleTimeString() })
  time_in: string;

  @Column({ nullable: true })
  time_out: string;

  @Column({ default: 0 })
  total_hour: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Employee, (employee) => employee.checkinout)
  @JoinColumn()
  employee: Employee;
  
  // @ManyToOne(() => SummarySalary, (summary_salary) => summary_salary.checkinout)
  // @JoinColumn()
  // summary_salary: SummarySalary;

  @OneToOne(() => SummarySalary, (summary_salary) => summary_salary.checkinout)
  summary_salary: SummarySalary;
}
