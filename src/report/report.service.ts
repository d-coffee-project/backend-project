import { Injectable } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportService {
  // constructor(@InjectDataSource() private dataSource: DataSource) {}
  // getProduct() {
  //   return this.dataSource.query('SELECT * FROM product');
  // }
  constructor(@InjectDataSource() private dataSource: DataSource) {}
  getEmployee() {
    return this.dataSource.query('CALL getEmployee');
  }

  getEmployee_info() {
    return this.dataSource.query('SELECT * FROM `getEmployee_info` ');
  }

  getMaterial() {
    return this.dataSource.query('CALL `getMaterial`');
  }
  // constructor(@InjectDataSource() private dataSource: DataSource) {}
  // getUser() {
  //   return this.dataSource.query('SELECT * FROM user');
  // }
  create(createReportDto: CreateReportDto) {
    return 'This action adds a new report';
  }

  findAll() {
    return `This action returns all report`;
  }

  findOne(id: number) {
    return `This action returns a #${id} report`;
  }

  update(id: number, updateReportDto: UpdateReportDto) {
    return `This action updates a #${id} report`;
  }

  remove(id: number) {
    return `This action removes a #${id} report`;
  }
}
