import { Module } from '@nestjs/common';
import { MaterialsService } from './materials.service';
import { MaterialsController } from './materials.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Material } from './entities/material.entity';
import { BillDetail } from 'src/bill_detail/entities/bill_detail.entity';
import { CheckMatDetail } from 'src/check_mat/entities/check_mat_detail';

@Module({
  imports: [TypeOrmModule.forFeature([Material, BillDetail, CheckMatDetail])],
  controllers: [MaterialsController],
  providers: [MaterialsService],
})
export class MaterialsModule {}
