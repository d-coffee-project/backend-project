import { BillDetail } from 'src/bill_detail/entities/bill_detail.entity';
import { CheckMatDetail } from 'src/check_mat/entities/check_mat_detail';
// import { CheckMaterialDetail } from 'src/check-material-detail/entities/check-material-detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  mat_id: number;

  @Column({
    length: 64,
  })
  mat_name: string;

  @Column()
  mat_min_quantity: number;

  @Column()
  mat_quantity: number;

  @Column({
    length: 64,
  })
  mat_unit: string;

  @Column()
  mat_price_per_unit: number;

  @CreateDateColumn({})
  createdDate: Date;

  @UpdateDateColumn({})
  updatedAt: Date;

  @DeleteDateColumn({})
  deletedAt: Date;

  @OneToMany(() => BillDetail, (bill_detail) => bill_detail.material)
  bill_detail: BillDetail;

  @OneToMany(() => CheckMatDetail, (checkMatDetail) => checkMatDetail.material)
  check_material_detail: CheckMatDetail[];
}
