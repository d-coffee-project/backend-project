import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TimeDw {
  @PrimaryGeneratedColumn()
  Time_Key: Date;

  @Column({ length: 64 })
  Year: string;

  @Column({ length: 255 })
  Quarter: string;

  @Column({ length: 255 })
  Month: string;

  @Column({ length: 255 })
  Week: string;

  @Column({ length: 255 })
  Day_of_Week: string;

  @Column({ length: 255 })
  Date: string;
}
