import { Module } from '@nestjs/common';
import { TimeDwService } from './time_dw.service';
import { TimeDwController } from './time_dw.controller';
import { StoreDw } from 'src/store_dw/entities/store_dw.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([StoreDw])],
  controllers: [TimeDwController],
  providers: [TimeDwService],
})
export class TimeDwModule {}
