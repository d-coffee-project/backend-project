import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class StoreDw {
  @PrimaryGeneratedColumn()
  store_id: number;

  @Column({ length: 255 })
  store_name: string;

  @Column({ length: 255 })
  store_subdistrict: string;

  @Column({ length: 255 })
  store_district: string;

  @Column({ length: 255 })
  store_city: string;
}
