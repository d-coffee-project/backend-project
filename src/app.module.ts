import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { StocksModule } from './stocks/stocks.module';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { Category } from './categories/entities/category.entity';
import { CategoriesModule } from './categories/categories.module';
import { AuthModule } from './auth/auth.module';
import { StoresModule } from './stores/stores.module';
import { Store } from './stores/entities/store.entity';
import { CheckInOutModule } from './check_in_out/check_in_out.module';
import { Material } from './materials/entities/material.entity';
import { MaterialsModule } from './materials/materials.module';
import { CheckInOut } from './check_in_out/entities/check_in_out.entity';
import { SummarySalaryModule } from './summary_salary/summary_salary.module';
import { ReportModule } from './report/report.module';
import { Report } from './report/entities/report.entity';
import { BillDetailModule } from './bill_detail/bill_detail.module';
import { BillModule } from './bill/bill.module';
import { Bill } from './bill/entities/bill.entity';
import { BillDetail } from './bill_detail/entities/bill_detail.entity';
import { CheckMatModule } from './check_mat/check_mat.module';
import { CheckMat } from './check_mat/entities/check_mat.entity';
import { CheckMatDetail } from './check_mat/entities/check_mat_detail';
import { CustomerDwModule } from './customer_dw/customer_dw.module';
import { EmployeeDwModule } from './employee_dw/employee_dw.module';
import { StoreDwModule } from './store_dw/store_dw.module';
import { TimeDwModule } from './time_dw/time_dw.module';
import { ProductDwModule } from './product_dw/product_dw.module';
import { CustomerDw } from './customer_dw/entities/customer_dw.entity';
import { EmployeeDw } from './employee_dw/entities/employee_dw.entity';
import { ProductDw } from './product_dw/entities/product_dw.entity';
import { StoreDw } from './store_dw/entities/store_dw.entity';
import { TimeDw } from './time_dw/entities/time_dw.entity';
import { FactDwModule } from './fact_dw/fact_dw.module';
import { FactDw } from './fact_dw/entities/fact_dw.entity';
import { SummarySalary } from './summary_salary/entities/summary_salary.entity';
import * as dotenv from 'dotenv';
import { ConfigModule } from '@nestjs/config';
dotenv.config();

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    // TypeOrmModule.forRoot(
    //   {
    //   type: 'mysql',
    //   host: 'db4free.net',
    //   port: 3306,
    //   username: 'dd_coffee',
    //   password: 'Pass@1234',
    //   database: 'dd_coffee_db',
    //   synchronize: true,
    //   migrations: [],
    //   entities: [Customer, Product, Order, OrderItem, User, Employee, Category],
    // }
    // ),
    TypeOrmModule.forRoot(
      //   {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   synchronize: false,
      //   migrations: [],
      //   entities: [Customer, Product, Order, OrderItem, User, Employee, Category],
      // }
      {
        type: 'mysql',
        host: process.env.DB_HOST,
        port: +process.env.DB_PORT,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        synchronize: true,
        entities: [
          Customer,
          Product,
          Order,
          OrderItem,
          User,
          Employee,
          Category,
          Store,
          Material,
          CheckInOut,
          SummarySalary,
          Report,
          Bill,
          BillDetail,
          CheckMat,
          CheckMatDetail,
          EmployeeDw,
          ProductDw,
          StoreDw,
          CustomerDw,
          TimeDw,
        ],
        logging: true,
      },
    ),
    CustomersModule,
    ProductsModule,
    OrdersModule,
    UsersModule,
    StocksModule,
    EmployeesModule,
    CategoriesModule,
    AuthModule,
    StoresModule,
    MaterialsModule,
    CheckInOutModule,
    StoresModule,
    MaterialsModule,
    CheckInOutModule,
    SummarySalaryModule,
    ReportModule,
    BillDetailModule,
    BillModule,
    CheckMatModule,
    CustomerDwModule,
    EmployeeDwModule,
    StoreDwModule,
    TimeDwModule,
    ProductDwModule,
    SummarySalaryModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
