import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common/decorators';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { Response } from 'express';
import { JwtService } from '@nestjs/jwt';

@UseGuards(JwtService)
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_imgs',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createProductDto: CreateProductDto,
    @UploadedFile() file: Express.Multer.File, //upload img เข้ามา
  ) {
    createProductDto.image = file.filename; //fileที่ถูก upload เข้ามา
    return this.productsService.create(createProductDto);
  }

  @Get('category/:id')
  findByCategory(@Param('id') id: string) {
    return this, this.productsService.findByCategory(+id);
  }

  @Get()
  findAll(@Query() query: { cat?: string; order?: string; orderBy?: string }) {
    return this.productsService.findAll({
      relations: ['category'],
      order: query.orderBy
        ? { [query.orderBy]: query.order }
        : { createdDate: 'ASC' },
      where: query.cat ? { categoryId: parseInt(query.cat) } : {},
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productsService.findOne(+id);
  }

  // แสดงรูป Product
  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const product = await this.productsService.findOne(+id);
    res.sendFile(product.image, { root: './product_imgs' });
  }
  //get product image ตามด้วย ชื่อ image //get ชื่อตรงๆ
  @Get('image/:imageFile')
  async getImageByFileName(
    @Param('imageFile') imageFile: string,
    @Res() res: Response,
  ) {
    res.sendFile(imageFile, { root: './product_imgs' });
  }

  // update image
  @Patch(':id/image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_imgs',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.productsService.update(+id, { image: file.filename });
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
    return this.productsService.update(+id, updateProductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productsService.remove(+id);
  }

  @Get('product/:category/:name')
  findByCategoryName(
    @Param('name') name: string,
    @Param('category') category: number,
  ) {
    return this, this.productsService.findByCategoryName(category,name);
  }
}
