import { IsNotEmpty, Length, IsPositive, IsString } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  price: number;

  // @IsString()
  // type: string;

  // @IsString()
  // size: string;

  image = 'search-empty.png';

  @IsNotEmpty()
  categoryId: number;
}
