import { IsNotEmpty, IsEmail } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  position: string;

  @IsNotEmpty()
  wage: number;

  image = 'no_img.png';
}
