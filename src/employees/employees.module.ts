import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { Employee } from './entities/employee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Bill } from 'src/bill/entities/bill.entity';
import { CheckMat } from 'src/check_mat/entities/check_mat.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, User, Bill, CheckMat])],
  controllers: [EmployeesController],
  providers: [EmployeesService],
})
export class EmployeesModule {}
