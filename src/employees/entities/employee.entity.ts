import { Bill } from 'src/bill/entities/bill.entity';
import { CheckInOut } from 'src/check_in_out/entities/check_in_out.entity';
import { User } from 'src/users/entities/user.entity';
import { Order } from 'src/orders/entities/order.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CheckMat } from 'src/check_mat/entities/check_mat.entity';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  address: string;

  @Column({
    length: '10',
  })
  tel: string;

  @Column({
    unique: true,
    length: '64',
  })
  email: string;

  @Column()
  position: string;

  @Column()
  wage: number;

  @Column({
    length: '128',
    default: 'no_img.png',
  })
  image: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => CheckInOut, (checkinout) => checkinout.employee)
  checkinout: CheckInOut[];

  @OneToMany(() => Order, (order) => order.employee)
  orders: Order[];

  @OneToOne(() => User, (user) => user.employee)
  @JoinColumn()
  user: User;

  @OneToMany(() => Bill, (bill) => bill.employee)
  bill: Bill;

  @OneToMany(() => CheckMat, (checkMat) => checkMat.employee)
  checkMats: CheckMat[];
}
