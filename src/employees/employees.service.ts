import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository, Like } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(User)
    private UsersRepository: Repository<User>,
  ) {}

  async create(createEmployeeDto: CreateEmployeeDto) {
    const emp = new Employee();
    // const userId = 3;
    // const userr = await this.UsersRepository.findOne({
    //   where: { id: userId, employee: { user: IsNull() } },
    //   relations: ['employee'],
    // });
    // console.log('user:: ', userr);
    // if (!userr) {
    //   throw new NotFoundException();
    // }
    emp.name = createEmployeeDto.name;
    emp.position = createEmployeeDto.position;
    emp.wage = createEmployeeDto.wage;
    emp.tel = createEmployeeDto.tel;
    emp.address = createEmployeeDto.address;
    emp.email = createEmployeeDto.email;
    emp.image = createEmployeeDto.image;
    const user = await this.UsersRepository.findOne({
      where: { email: createEmployeeDto.email ,role: createEmployeeDto.position },
    });
    if (!user) {
      throw new NotFoundException();
    }
    emp.user = user;

    return this.employeesRepository.save(emp);
  }

  findAll() {
    return this.employeesRepository.find({ relations: ['user'] });
  }

  async findOne(id: number) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
      // relations: ['orders'],
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return employee;
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    const updatedEmployee = { ...employee, ...updateEmployeeDto };
    console.log('updateEmployeeDto:: ', updateEmployeeDto);
    console.log('updatedEmployee:: ', updatedEmployee);
    return this.employeesRepository.save(updatedEmployee);
  }

  async updateImg(
    id: number,
    updateEmployeeDto: UpdateEmployeeDto,
    fileName: string,
  ) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    const updatedEmployee = { ...employee, ...updateEmployeeDto };
    if (fileName) {
      //fileName = 'no_img.png'
      updatedEmployee.image = fileName;
    }
    return this.employeesRepository.save(updatedEmployee);
  }

  async remove(id: number) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return this.employeesRepository.remove(employee);
  }

  findByName(name: string) {
    console.log("name:: ",name)
    return this.employeesRepository.find({
      where: { name: Like(`%${name}%`) },
    });
  }

}

// **ขา create**
// - save sucess /
// - upload file /
// - display file /

// **ขา update**
// - save success/
// - เปลี่ยน patch เป็น post เพื่อ upload file ทุกครั้งที่มีการ upload file /
// - กรณีเคย upload file ไปแล้ว และไม่มีการ upload ใหม่ รูปเดิมจะไม่ถูกลบ ****popup ไม่โชว์ชื่อ อาจต้องแก้เพื่อความ make sence
