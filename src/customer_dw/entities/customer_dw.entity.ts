import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class CustomerDw {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  customer_name: string;

  @CreateDateColumn()
  customer_start_date: Date;
}
