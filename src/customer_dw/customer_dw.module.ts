import { Module } from '@nestjs/common';
import { CustomerDwService } from './customer_dw.service';
import { CustomerDwController } from './customer_dw.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CustomerDw } from './entities/customer_dw.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CustomerDw])],
  controllers: [CustomerDwController],
  providers: [CustomerDwService],
})
export class CustomerDwModule {}
