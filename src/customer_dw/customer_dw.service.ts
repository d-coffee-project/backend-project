import { Injectable } from '@nestjs/common';
import { CreateCustomerDwDto } from './dto/create-customer_dw.dto';
import { UpdateCustomerDwDto } from './dto/update-customer_dw.dto';

@Injectable()
export class CustomerDwService {
  create(createCustomerDwDto: CreateCustomerDwDto) {
    return 'This action adds a new customerDw';
  }

  findAll() {
    return `This action returns all customerDw`;
  }

  findOne(id: number) {
    return `This action returns a #${id} customerDw`;
  }

  update(id: number, updateCustomerDwDto: UpdateCustomerDwDto) {
    return `This action updates a #${id} customerDw`;
  }

  remove(id: number) {
    return `This action removes a #${id} customerDw`;
  }
}
