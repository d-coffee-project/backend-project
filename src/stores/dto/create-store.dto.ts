import { IsNotEmpty, IsString, IsPhoneNumber, Length } from 'class-validator';

export class CreateStoreDto {
  @IsNotEmpty()
  @IsString()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  @Length(3, 255)
  address: string;

  @IsNotEmpty()
  @Length(10, 10)
  @IsPhoneNumber('TH')
  tel: string;
}
