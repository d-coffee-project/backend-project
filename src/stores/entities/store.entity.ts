import { Order } from 'src/orders/entities/order.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Store {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64 })
  name: string;

  @Column({ length: 255 })
  address: string;

  @Column({ length: 10 })
  tel: string;

  @OneToMany(() => Order, (order) => order.store)
  orders: Order[];
}
